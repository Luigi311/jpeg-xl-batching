#!/bin/bash

SPEED=8
THREADS=1
OUTPUT="output"

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

while :; do
    case "$1" in
        -h | -\? | --help)
            printf 'Convert an image to jpeg-xl\n'
            printf '\nUsage:\n'
            printf '\t ./encoder.sh [options]\n'
            printf '\nExample:\n'
            printf '\t ./encoder.sh --file photos --workers 12'
            printf '\n\nGeneral Options:\n'
            printf '\t -i/--input   [file]\t\t Images to convert\n'
            printf '\t -o/--output  [folder]\t\t Output folder to place all encoded images and stats files (default output)\n'
            printf '\nEncoding Settings:\n'
            printf '\t -q/--quality [number]\t\t Quality to encode image at\n'
            printf '\t -t/--threads [number]\t\t Amount of threads each encode should use (default 4)\n'
            printf '\t -s/--speed   [number]\t\t Speed for encoding (default 8)\n'
            printf '\t -f/--flags   [flags]\t\t Flags to pass to cjpegxl, surround in quotes for multiple flags\n'
            exit 0
            ;;
        -i | --input)
            if [[ "$2" ]]; then
                INPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -o | --output)
            if [[ "$2" ]]; then
                OUTPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -q | --quality)
            if [[ "$2" ]]; then
                QUALITY="-q $2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -t | --threads)
            if [[ "$2" ]]; then
                THREADS="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -s | --speed)
            if [[ "$2" ]]; then
                SPEED="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -f | --flags)
           if [[ "$2" ]]; then
                FLAGS="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --debug)
            set -x
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            die "Error: Unknown option : $1"
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

OUTFILE=$(basename "$INPUT" .png).jxl
CSV=$(basename "$INPUT" .png).csv

cjpegxl "$INPUT" "$OUTPUT/$OUTFILE" $QUALITY -s "$SPEED" --num_threads "$THREADS" $FLAGS > /dev/null 2>&1 &&
printf '%s\t%s' "$OUTFILE" "$(du "$OUTPUT"/"$OUTFILE" | cut -f -1)" > "$OUTPUT/$CSV"
