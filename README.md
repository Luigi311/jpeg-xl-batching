# jpeg-xl-batching

Convert a folders worth of images to jpeg-xl utilizing gnu parallel to allow for multiple jobs to be ran at the same time

## Usage
```bash
$ ./run.sh [options]
```

## Example
```bash
$ ./run.sh --input ~/photos --workers 12
```

## Options
```bash
General Options:
    -i/--input   [folder]          Folder containing images to convert (default photos)
    -o/--output  [folder]          Output folder to place all encoded photos and stats files (default output)   
    -w/--workers [number]          Number of encodes to run simultaneously (defaults cpu threads/threads)
    --resume                       Resume option for parallel, will use jpegxl.log to know what has been encoded (default false)

Encoding Settings:
    -q/--quality [number]          Quality to encode image at
    -t/--threads [number]          Amount of threads each encode should use (default 4)
    -s/--speed   [number]          Jpegxl encoding speed setting (default 8)
    -f/--flags   [flags]           Flags to pass to cjpegxl, surround in quotes for multiple flags
```
