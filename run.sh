#!/usr/bin/env bash
set -e
set -o pipefail

SPEED=8
THREADS=4
MANUAL=0
OUTPUT="output"
INPUT="photos"

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

while :; do
    case "$1" in
        -h | -\? | --help)
            printf 'Convert a folders worth of images to jpeg-xl utilizing gnu parallel to allow for multiple jobs to be ran at the same time\n'
            printf '\nUsage:\n'
            printf '\t ./run.sh [options]\n'
            printf 'Example:\n'
            printf '\t ./run.sh --input ~/photos --workers 12'
            printf '\nGeneral Options:\n'
            printf '\t -i/--input   [folder]\t\t Folder containing images to convert (default photos)\n'
            printf '\t -o/--output  [folder]\t\t Output folder to place all encoded images and stats files (default output)\n'
            printf '\t -w/--workers [number]\t\t Number of encodes to run simultaneously (defaults cpu threads/threads)\n'
            printf '\t --resume\t\t\t Resume option for parallel, will use jpegxl.log to know what has been encoded (default false) \n'
            printf '\nEncoding Settings:\n'
            printf '\t -q/--quality [number]\t\t Quality to encode image at\n'
            printf '\t -t/--threads [number]\t\t Amount of threads each encode should use (default 4)\n'
            printf '\t -s/--speed   [number]\t\t Jpegxl encoding speed setting (default 8)\n'
            printf '\t -f/--flags   [flags]\t\t Flags to pass to cjpegxl, surround in quotes for multiple flags'
            exit 0
            ;;
        -i | --input)
            if [[ "$2" ]]; then
                INPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -o | --output)
            if [[ "$2" ]]; then
                OUTPUT="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -w | --workers)
            if [[ "$2" ]]; then
                WORKERS="$2"
                MANUAL=1
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --resume)
            RESUME="--resume"
            ;;
        -q | --quality)
            if [[ "$2" ]]; then
                QUALITY="--quality $2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -t | --threads)
            if [[ "$2" ]]; then
                THREADS="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -s | --speed)
            if [[ "$2" ]]; then
                SPEED="$2"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        -f | --flags)
            if [[ "$2" ]]; then
                FLAGS="--flags '$2'"
                shift
            else
                die "ERROR: $1 requires a non-empty option argument."
            fi
            ;;
        --debug)
            set -x
            DEBUG="--debug"
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            die "Error: Unknown option : $1"
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

if [[ "$MANUAL" -ne 1 ]]; then
  WORKERS=$(( $(nproc) / THREADS ))
fi

mkdir -p "$OUTPUT"
find "$INPUT" -name "*.png" | parallel -j "$WORKERS" --joblog jpegxl.log $RESUME --bar ./scripts/encoder.sh --input {} --output "$OUTPUT" $QUALITY --speed "$SPEED" --threads "$THREADS" $FLAGS $DEBUG 
